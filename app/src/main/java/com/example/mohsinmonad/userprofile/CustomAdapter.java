package com.example.mohsinmonad.userprofile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter {
    Context mContext;
    int resourceID;
    ArrayList<String> names;
    public CustomAdapter(Context context, int resource, ArrayList<String> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.resourceID=resource;
        this.names= objects;
    }

    @Override
    public String getItem(int position) {
        return names.get(position);
    }

    @NonNull
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        row = inflater.inflate(R.layout.names_view, parent, false);

        TextView text = row.findViewById(R.id.text);

        text.setText(names.get(position));
        return row;

    }

}