package com.example.mohsinmonad.userprofile;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity  {

    Button kalabagan, mohammadpur;
    Button newmarket, dhanmodi;

    @SuppressLint("CutPasteId")
    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        kalabagan = findViewById(R.id.btn_kbagan);
        mohammadpur = findViewById(R.id.btn_mpur);
        newmarket = findViewById(R.id.btn_newmarket);
        dhanmodi = findViewById(R.id.btn_dhanmondi);

        ArrayList<String> categories = new ArrayList();
        categories.add("Green Road");
        categories.add("Sukrabad");
        categories.add("Panthopath");

        kalabagan.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.customwindow);
            dialog.setTitle("Area List");
            ListView listView = dialog.findViewById(R.id.listView1);
            CustomAdapter adapter = new CustomAdapter(getApplicationContext(), 10, categories);
            listView.setAdapter(adapter);
            dialog.show();

        });

        mohammadpur.setOnClickListener(v ->
                startActivity(new Intent(getApplicationContext(),AgentMemberActivity.class)));

        newmarket.setOnClickListener(v ->
                startActivity(new Intent(getApplicationContext(),AgentActivity.class)));


        dhanmodi.setOnClickListener(v ->
                startActivity(new Intent(getApplicationContext(),Contact_detailActivity.class)));
    }

}